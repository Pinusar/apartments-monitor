from main_logic import find_apartments
import urllib.parse
from time import sleep

min_rooms = input("Enter minimum number of rooms: ")
while min_rooms == "" or not min_rooms.isdigit():
    min_rooms = input("Invalid input. Please enter minimum number of rooms: ")
max_rooms = input("Enter maximum number of rooms: ")
while max_rooms == "" or not max_rooms.isdigit():
    max_rooms = input("Invalid input. Please enter maximum number of rooms: ")
max_price = input("Enter maximum price (leave blank to skip): ")
if max_price.isdigit():
    max_price_int = int(max_price.replace(" ", ""))
else:
    max_price_int = None
max_price = urllib.parse.quote(max_price)
keyword = urllib.parse.quote(input("Enter search keyword (e.g. address, street or other keyword. Leave blank to skip): "))
requirements = {"min_rooms":min_rooms, "max_rooms": max_rooms, "max_price": max_price, "keyword": keyword, "max_price_int": max_price_int}

cycle = 1
while True:
    print("Starting cycle " + str(cycle))
    find_apartments(requirements)
    print("End of cycle " + str(cycle))
    sleep(600)
    cycle += 1
