from sqlalchemy import create_engine, Table, MetaData, select


def insert_apartments(apartments_lst):
    db_engine = create_engine("sqlite:///db.sqlite3")
    connection = db_engine.connect()
    metadata = MetaData()
    apartments = Table('Apartment', metadata, autoload=True, autoload_with=db_engine)
    for apartment in apartments_lst:
        insert = apartments.insert()
        connection.execute(insert, id=apartment.id, address=apartment.address, description=apartment.description,
                           rooms=int(apartment.rooms), area=int(apartment.area),
                           price=apartment.price, pricem2=int(apartment.pricem2))


def get_apartment_ids():
    db_engine = create_engine("sqlite:///db.sqlite3")
    connection = db_engine.connect()
    metadata = MetaData()
    apartments = Table('Apartment', metadata, autoload=True, autoload_with=db_engine)
    s = select([apartments])
    result = connection.execute(s)
    apartment_ids = []
    for row in result:
        apartment_ids.append(row["id"])
    return apartment_ids

