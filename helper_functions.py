from apartment import Apartment
import urllib.parse


def price_to_int(price):
    """Convert raw text price to integer in euros."""
    x = str(price)[:-2]
    x = x.replace(u'\xa0', '')
    x = x.replace(" ", '')
    y = int(x)
    return y


def area_to_int(area):
    """Convert raw text area to integer."""
    x = str(area)[:-2]
    x = x.replace(u'\xa0', '')
    x = x.replace(" ", '')
    y = int(float(x))
    return y


def price_m2_to_int(price):
    """Convert raw text price per m2 to integer in euros."""
    x = str(price)[:-4]
    x = x.replace(u'\xa0', '')
    x = x.replace(" ", '')
    y = int(float(x))
    return y


def find_suitable_apartments(tags, max_price=None):
    """Find suitable apartments by max price. Tags is a list of BeautifulSoup tags."""
    suitable_apartments = []
    for tag in tags:
        address = tag.find(class_="object-title-a")
        if address:
            address = tag.find(class_="object-title-a").text.strip()
        else:
            continue
        description = tag.find(class_="object-important-note")
        if description:
            description = tag.find(class_="object-important-note").text.strip()
        else:
            description = "No description"
        id = int(tag["id"])
        nr_of_rooms = int(tag.find(class_="object-rooms").text)
        area = tag.find(class_="object-m2").text.strip()
        area = area_to_int(area)
        price = tag.find(class_="object-price-value").text.strip()
        int_price = price_to_int(price)
        price_per_m2 = tag.find(class_="object-m2-price").text.strip()
        price_per_m2 = price_m2_to_int(price_per_m2)
        apartment = Apartment(id, address, description, nr_of_rooms, area, int_price, price_per_m2)
        if not max_price or apartment.price <= max_price:
            suitable_apartments.append(apartment)
    return suitable_apartments


def build_search_url(requirements):
    min_rooms = requirements["min_rooms"]
    max_rooms = requirements["max_rooms"]
    max_price = requirements["max_price"]
    keyword = requirements["keyword"]
    url = f"https://www.kv.ee/?act=search.simple&last_deal_type=1&company_id=&page=1&orderby=cdwl&page_size=100&deal_type=1&dt_select=1" \
                   "&county=0" \
                   "&search_type=old" \
                   "&parish=" \
                   f"&rooms_min={min_rooms}" \
                   f"&rooms_max={max_rooms}" \
                   "&price_min=&" \
                   f"price_max={max_price}" \
                   "&nr_of_people=" \
                   "&area_min=" \
                   "&area_max=" \
                   "&floor_min=" \
                   "&floor_max=" \
                   "&energy_certs=" \
                   f"&keyword={keyword}"
    return url
