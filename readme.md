## Overview
This is a program for monitoring apartment offers on kv.ee portal. You can specify desired parameters and whenever a suitable apartment offer is entered, you will get an SMS notification.
The program checks for new apartments every 10 minutes. If there are too many new offers to send via sms, you will just receive an sms that there are too many new offers.

## Setup

* Activate virtual environment 
>source venv/bin/activate

* Install dependencies
> pip install -r requirements.txt

* Run db_init.py to create the database.

* Add your Twilio credentials in sms.py

* Use command_line.py to run the program on command line.
