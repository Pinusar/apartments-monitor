from sqlalchemy import create_engine, Table, String, MetaData, Column, Integer

engine = create_engine("sqlite:///db.sqlite3", echo=True)

metadata = MetaData()

apartments = Table("Apartment", metadata,
                   Column("id", Integer, primary_key=True),
                   Column("address", String),
                   Column("description", String),
                   Column("rooms", Integer),
                   Column("area", Integer),
                   Column("price", Integer),
                   Column("pricem2", Integer)
                   )

metadata.create_all(engine)

ins = apartments.insert()

conn = engine.connect()