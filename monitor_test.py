from bs4 import BeautifulSoup
from apartment import Apartment
from helper_functions import price_to_int, find_suitable_apartments, build_search_url
import pytest

with open("sample.html", "r") as f:
    html = f.read()

soup = BeautifulSoup(html, 'html.parser')

apartments = soup.find_all(class_="object-type-apartment")


def test_find_apartment_by_price():
    suitable_apartments = find_suitable_apartments(apartments, 31000)
    id = suitable_apartments[0].id
    assert id == '3239261'


def test_price_to_int_spaces():
    price_string = "1 000 203 £"
    price = price_to_int(price_string)
    assert price == 1000203


def test_price_to_int_no_spaces():
    price_string = "1000203 £"
    price = price_to_int(price_string)
    assert price == 1000203
