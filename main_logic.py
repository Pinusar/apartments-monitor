from bs4 import BeautifulSoup
from requests import get
from apartment import Apartment
from helper_functions import find_suitable_apartments, build_search_url
from db import insert_apartments, get_apartment_ids
from sms import send_sms


def find_apartments(requirements):

    url = build_search_url(requirements)

    response = get(url)

    soup = BeautifulSoup(response.content, 'html.parser')

    apartments = soup.find_all(class_="object-type-apartment")

    suitable_apartments = find_suitable_apartments(apartments, requirements["max_price_int"])

    apartment_ids = get_apartment_ids()

    sms_text = ""

    for apartment in suitable_apartments:
        if apartment.id not in apartment_ids:
            insert_apartments([apartment])
            sms_text += "New object:\n" + str(apartment) + "\n"
        else:
            print("Apartment already in database.\n=====")

    if sms_text:
        if len(sms_text) > 800:
            sms_text = "Too many apartments. Message too long."
        send_sms(sms_text)
        print("SMS SENT!")