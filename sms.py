from twilio.rest import Client


def send_sms(content):
    account_sid = 'xxxxx'
    auth_token = 'xxxxx'
    client = Client(account_sid, auth_token)

    message = client.messages \
        .create(
             body=content,
             from_='+xxxxx',
             to='+xxxxx'
         )
