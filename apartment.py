class Apartment:
    def __init__(self, id, address, description, rooms, area, price, pricem2):
        self.id = id
        self.address = address
        self.description = description
        self.rooms = rooms
        self.area = area
        self.price = price
        self.pricem2 = pricem2

    def __str__(self):
        return f"Apartment object.\nID: {self.id}\nAddress: {self.address}\n{self.description}\nNr of rooms: {self.rooms}, Area: {self.area}\nPrice: {self.price}\nPrice per m2: {self.pricem2}"
